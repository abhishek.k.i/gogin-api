package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

func HomePage(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "get home page",
	})
}
func PostHomePage(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "post home page",
	})
}
func QueryStrings(c *gin.Context) {
	name := c.Query("name")
	age := c.Query("age")

	c.JSON(200, gin.H{
		"name": name,
		"age":  age,
	})
}

func main() {

	fmt.Println("HELLOO WORLD")
	r := gin.Default()
	r.GET("/", HomePage)
	r.POST("/", PostHomePage)
	r.GET("/query", QueryStrings)
	r.Run()

}
